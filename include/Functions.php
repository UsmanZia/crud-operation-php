<?php
function Query_Set($query)
{
	if(!$query)
 {
	 global $connection;
	 die("Query is failed".mysqli_error($connection));
 }
}
function Confirm_Login()
{
	if(!isset($_SESSION["adminuser"]))
  {
	  redirect_to("Admin_Login.php");
  }
}
function redirect_to($new_location)
{
	header("Location: " . $new_location);
	  exit;
}
function mysql_prom($string)
{
	global $connection;
	$escape_string=mysqli_real_escape_string($connection,$string);
	return $escape_string;
}
function Find_all_Subject($public=true)
{
     global $connection;	
	 $query="  SELECT * ";
	 $query.=" FROM subjects ";
	  if($public)
	  {
	 $query.=" WHERE Visible=1 ";
	  }
	 $query.=" ORDER BY Position ASC";
	 $Subject_res=mysqli_query($connection,$query);
	 Query_Set($Subject_res);
	 return $Subject_res;
}
function Find_Page_From_Subject($Subject_id ,$public=true)
{
       global $connection;		
	   $query="  SELECT * ";
	   $query.=" FROM page ";
	   $query.=" WHERE Subject_ID={$Subject_id} ";
	   if($public)
	   {
	   $query.=" AND Visible=1 ";
	   }
	   $query.="ORDER BY Position ASC";
	   $Page_res=mysqli_query($connection,$query);
	   Query_Set($Page_res);
	   return $Page_res;
}
//Its Fing Subject By id its exit or not
function Find_Subject_By_Id($subject_id)
{
	   global $connection;
	    $Safe_Sql_ID=mysqli_real_escape_string($connection,$subject_id);
	   $query="  SELECT * ";
	   $query.=" FROM subjects ";
		$query.=" WHERE ID={$Safe_Sql_ID} ";

	   //$query.=" AND Visible=1 ";
	   $query.="LIMIT 1";
	   $Subject_Set=mysqli_query($connection,$query);
	   Query_Set($Subject_Set);
	   if($subject=mysqli_fetch_assoc($Subject_Set))
	   {
	   return $subject;
	   }
	   else
	   {
		   return null;
	   }
}
//Its Fing page By id its exit or not
function Find_Page_By_Id($page_id,$public=true)
{
	 global $connection;
	    $Safe_Sql_ID=mysqli_real_escape_string($connection,$page_id);
	   $query="  SELECT * ";
	   $query.=" FROM page ";
	   $query.=" WHERE ID={$Safe_Sql_ID} ";
	   if($public)
	   {
	  $query.=" AND Visible=1 ";
	   }
	   
	   $query.="LIMIT 1";
	   $Page_Set=mysqli_query($connection,$query);
	   Query_Set($Page_Set);
	   if($Page=mysqli_fetch_assoc($Page_Set))
	   {
	   return $Page;
	   }
	   else
	   {
		   return null;
	   }
}
//this function takes two arguments on page id and subject id and show all pages and subject
function Navigation($subject_array,$page_array)
  {
	    $output="<ul class=\"nav ul_style\">";
	  //Function Call And Find All the Subjects
		$Subject_res=Find_all_Subject(false);
		//subject show using while loop
		while($Subjects=mysqli_fetch_assoc($Subject_res))
		{
			$output.="<li";
				if($subject_array && $Subjects["ID"]==$subject_array["ID"])
				{
				  $output.=" class=\"Selected\"";
				}
			$output.=">";
			$output.="<a href=\"manage_content.php?Subject=";
		    $output.= urlencode($Subjects["ID"]);
			$output.="\">";
			$output.=htmlentities($Subjects["Subject_Name"]);
			$output.="</a>";   
		   //Function Call And Find All the Page From Releated
			$Page_res=Find_Page_From_Subject($Subjects["ID"] , false);
			$output.="<ul class=\"nav ul_style\">";
			while($pages=mysqli_fetch_assoc($Page_res))
				{
					 $output.= "<li";
					 if($page_array && $pages["ID"]==$page_array["ID"])
					 {
					   $output.= " class=\"Selected\"";
					 }
					 $output.= ">";
					 $output.="<a href=\"manage_content.php?Page=";
					 $output.= urlencode($pages["ID"]);
					 $output.="\">";
					 $output.= htmlentities($pages["menu_name"]);
					 $output.="</a>";
					 $output.="</li>";
				}
			$output.="</ul>";
			mysqli_free_result($Page_res);	  
			$output.="</li>";	  
			   }//while loop close of subject show
   mysqli_free_result($Subject_res);
	$output.=" </ul>";
        return $output;
}
function Public_Navigation($subject_array,$page_array)
  {
	    $output="<ul class=\"nav ul_style\">";
	  //Function Call And Find All the Subjects
		$Subject_res=Find_all_Subject();
		//subject show using while loop
		while($Subject=mysqli_fetch_assoc($Subject_res))
		{
			$output.="<li";
				if($subject_array && $Subject["ID"]==$subject_array["ID"])
				{
				  $output.=" class=\"Selected\"";
				}
			$output.=">";
			$output.="<a href=\"index.php?Subject=";
		    $output.= urlencode($Subject["ID"]);
			$output.="\">";
			$output.=htmlentities($Subject["Subject_Name"]);
			$output.="</a>";   
			if($subject_array["ID"]==$Subject["ID"] || $page_array["Subject_ID"]==$Subject["ID"])
			{
		   //Function Call And Find All the Page From Releated
			$Page_res=Find_Page_From_Subject($Subject["ID"]);
			$output.="<ul class=\"nav ul_style\">";
			while($page=mysqli_fetch_assoc($Page_res))
				{
					 $output.= "<li";
					 if($page_array &&$page["ID"]==$page_array["ID"])
					 {
					   $output.= " class=\"Selected\"";
					 }
					 $output.= ">";
					 $output.="<a href=\"index.php?Page=";
					 $output.= urlencode($page["ID"]);
					 $output.="\">";
					 $output.= htmlentities($page["menu_name"]);
					 $output.="</a>";
					 $output.="</li>";
				}
			$output.="</ul>";
			mysqli_free_result($Page_res);	
			}			
			$output.="</li>";	  
			   }//while loop close of subject show
   mysqli_free_result($Subject_res);
	$output.=" </ul>";
        return $output;
}
function Find_Dafault_page_by_Subject($Subject_id)
{
	$First_Page=Find_Page_From_Subject($Subject_id);
	if($page=mysqli_fetch_assoc($First_Page))
	{
		return $page;
	}
	else
	{
		return null;
	}
}
//Return two global argument
//one is Current_Subject
//second is Current_Page
function find_Selected_page($public=false)
{
	global $Current_Subject;
	global $Current_Page;
	if(isset($_GET["Subject"]) && !empty($_GET["Subject"]))
       {
         echo "fff";
	  $Current_Subject=Find_Subject_By_Id($_GET["Subject"]);
	  if($public)
	  {
	  $Current_Page=Find_Dafault_page_by_Subject($Current_Subject["ID"]);
	  }
	  else
	  {
		  $Current_Page=null;
	  }
       }
  else if(isset($_GET["Page"]) && !empty($_GET["Page"]))
      {

		 $Current_Page=Find_Page_By_Id($_GET["Page"] ,$public);
		 $Current_Subject=null;
      }
  else
  {
	$Current_Subject=null; 
    $Current_Page=null;	
  }
}
function Show_pages_By_Subject_ID($Subject_ID)
{
	$page_result=Find_Page_From_Subject($Subject_ID);
	$output=null;
	if(!is_null($page_result))
	{
	  $output.="<h1>Page in this Subject</h1>";
	}
	$output.="<ul class=\"nav ul_style\">";
	while($page=mysqli_fetch_assoc($page_result))
	  {
		 $output.= "<li>";
		 $output.="<a href=\"manage_content.php?Page=";
		 $output.= urlencode($page["ID"]);
		 $output.="\">";
		 $output.= htmlentities($page["menu_name"]);
		 $output.="</a>";
		 $output.="</li>";
		}
		$output.="</ul>";
		mysqli_free_result($page_result);
	return $output;
}
//Admin area
function Find_All_Admin()
{
	global $connection;	
	 $query="  SELECT * ";
	 $query.=" FROM admin ";
	 $query.=" ORDER BY UserName ASC";
	 $Admin_res=mysqli_query($connection,$query);
	 Query_Set($Admin_res);
	 return $Admin_res;
}
function find_admin_by_id($admin_id)
{
	global $connection;
	    $Safe_Sql_ID=mysqli_real_escape_string($connection,$admin_id);
	   $query="  SELECT * ";
	   $query.=" FROM admin ";
	   $query.=" WHERE ID={$Safe_Sql_ID} ";
	   $query.="LIMIT 1";
	   $admin_Set=mysqli_query($connection,$query);
	   Query_Set($admin_Set);
	   if($Admin=mysqli_fetch_assoc($admin_Set))
	   {
	   return $Admin;
	   }
	   else
	   {
		   return null;
	   }
}
function password_eccript($password)
{
	  $hash_format="$2y$10$";
	  $salt_length=22;//Its always 22 length or more;
	  $salt=generate_salt($salt_length);
	  $format_and_salt=$hash_format.$salt;
	  $hash=crypt($password ,$format_and_salt);
	  return $hash;
}
function generate_salt($length)
{
	// Not 100% unique, not 100% random, but good enough for a salt
	  // MD5 returns 32 characters
	$unique_random_string=md5(uniqid(mt_rand(),true));
	// Valid characters for a salt are [a-zA-Z0-9./]
	 $base64_string=base64_encode($unique_random_string);
	 // But not '+' which is valid in base64 encoding
	 $modifed_base64_string=str_replace('+', '.' ,$base64_string);
	 // Truncate string to the correct length
	 $salt=substr($modifed_base64_string,0,$length);
	 return $salt;
}
function password_Checked($password,$exiting_hash)
{
	$hash=crypt($password,$exiting_hash);
	if($hash==$exiting_hash)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function attempt_login($username ,$userpassword)
{
	$admin=Find_Admin_By_UserName($username);
	if($admin)
	{
		if(password_Checked($userpassword,$admin["UserPassword"]))
		{
			return $admin;
		}
		else{
			return false;
		}
	}
	else
	{
		return null;
	}
}
function Find_Admin_By_UserName($username)
{
	    global $connection;
	    $Safe_Sql_ID=mysqli_real_escape_string($connection,$username);
	    $query="  SELECT * ";
	    $query.=" FROM admin ";
		$query.=" WHERE UserName='{$Safe_Sql_ID}' ";
	    $query.="LIMIT 1";
	    $result=mysqli_query($connection,$query);
	    Query_Set($result);
	    if($admin=mysqli_fetch_assoc($result))
	    {
	    return $admin;
	    }
	    else
	     {
		   return false;
	     }
}
?>