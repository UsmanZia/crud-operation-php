<?php require_once("../include/Session.php")?>
<?php require_once("../include/basic_connection.php")?>
<?php require_once("../include/Functions.php")?>
<?php Confirm_Login();?>
<?php 
  $Admin=find_admin_by_id($_GET["id"]);
  if(!$Admin)
  {
	   redirect_to("manage_admin_user.php");
  }
    $id=$Admin["ID"];
	$query="DELETE FROM admin";
	$query.=" WHERE ID={$id}";
     $result=mysqli_query($connection,$query);
if($result && mysqli_affected_rows($connection)>=0)
{
	$_SESSION["message"]="Admin is Deleted..";
	redirect_to("manage_admin_user.php");
}else{
	Query_Set($result);
	redirect_to("manage_admin_user.php");
}
?>
<?php
if(isset($connection)){mysqli_close($connection);}?>